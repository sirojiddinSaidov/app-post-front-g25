import axios from "axios";
import {useEffect, useState} from "react";

const Post = () => {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getPosts()
    }, [])

    const getPosts = () => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(javob => {
                setPosts(javob.data)
            })
    }


    return (
        <div>
            {/*<button onClick={getPosts}>Bor</button>*/}
            <table>
                <tr>
                    <th>TR</th>
                    <th>Title</th>
                    <th>Body</th>
                </tr>


                {posts.map((post, i) =>
                    <tr>
                        <td>{i + 1}</td>
                        <td>{post.title}</td>
                        <td>{post.body}</td>
                    </tr>
                )}
            </table>
        </div>
    )
}

export default Post;