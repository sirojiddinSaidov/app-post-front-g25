import {useState} from "react";

const Ketmon = () => {

    const [number, setNumber] = useState(0);

    const dec = () => {
        setNumber(number - 1)
    }

    const inc = () => {
        setNumber(number + 1)
    }

    return (
        <div>
            <button onClick={dec}>-</button>
            <h1 id={Math.random()}>{number}</h1>
            <button onClick={inc}>+</button>
        </div>
    )

}

export default Ketmon;